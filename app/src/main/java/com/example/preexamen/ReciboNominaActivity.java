package com.example.preexamen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.text.DecimalFormat;

public class ReciboNominaActivity extends AppCompatActivity {

    private EditText txtHorasTrabajadas;
    private EditText txtHorasExtras;
    private RadioGroup radioGroupPuesto;
    private TextView txtPagoSubtotal;
    private TextView txtPagoImpuesto;
    private TextView txtTotalPago;

    private ReciboNomina cot;
    private TextView txtNumeroRecibo;

    private TextView txtUsuario;

    private RadioButton auxiliar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_recibo_nomina);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        cot = new ReciboNomina();
        txtUsuario = findViewById(R.id.txtUsuario);
        auxiliar = findViewById(R.id.auxiliar);
        txtNumeroRecibo = findViewById(R.id.txtNumeroRecibo);

        txtNumeroRecibo.setText("Numero de recibo: "+String.valueOf(cot.generaId()));
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("Cliente");
        txtUsuario.setText("Usuario: "+ nombre);
        auxiliar.setSelected(true);

        // Bind views
        txtHorasTrabajadas = findViewById(R.id.txtHorasTrabajadas);
        txtHorasExtras = findViewById(R.id.txtHorasExtras);
        radioGroupPuesto = findViewById(R.id.radioGroupPuesto);
        txtPagoSubtotal = findViewById(R.id.txtPagoSubtotal);
        txtPagoImpuesto = findViewById(R.id.txtPagoImpuesto);
        txtTotalPago = findViewById(R.id.txtTotalPago);

        Button btnCalcular = findViewById(R.id.BtnCalcular);
        Button btnLimpiar = findViewById(R.id.BtnLimpiar);
        Button btnCerrar = findViewById(R.id.BtnCerrar);

        // Set onClickListeners
        btnCalcular.setOnClickListener(this::calcular);
        btnLimpiar.setOnClickListener(this::limpiar);
        btnCerrar.setOnClickListener(this::cerrar);
    }

    private void calcular(View view) {

        if(txtHorasTrabajadas.getText().toString().isEmpty()|| txtHorasExtras.getText().toString().isEmpty()){
            Toast.makeText(ReciboNominaActivity.this, "No has ingresado los datos", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            int horasNormales = Integer.parseInt(txtHorasTrabajadas.getText().toString());
            int horasExtras = Integer.parseInt(txtHorasExtras.getText().toString());
            int puesto = getPuestoSeleccionado();
            double porcentajeImpuesto = 16.0;

            ReciboNomina recibo = new ReciboNomina(1, "Usuario", horasNormales, horasExtras, puesto, porcentajeImpuesto);

            double subtotal = recibo.calcularSubtotal();
            double impuesto = recibo.calcularImpuesto();
            double totalAPagar = recibo.calcularTotalAPagar();


            txtPagoSubtotal.setText("Subtotal: $" + String.format("%.2f", subtotal));
            txtPagoImpuesto.setText("Impuesto: $" + String.format("%.2f", impuesto));
            txtTotalPago.setText("Total a pagar: $" + String.format("%.2f", totalAPagar));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    private void limpiar(View view) {
        txtHorasTrabajadas.setText("");
        txtHorasExtras.setText("");
        radioGroupPuesto.check(R.id.auxiliar);
        txtPagoSubtotal.setText("Subtotal: $");
        txtPagoImpuesto.setText("Impuesto: $");
        txtTotalPago.setText("Total a pagar: $");
    }

    private void cerrar(View view) {
        finish();
    }

    private int getPuestoSeleccionado() {
        int selectedId = radioGroupPuesto.getCheckedRadioButtonId();
        if (selectedId == R.id.auxiliar) {
            return 1;
        } else if (selectedId == R.id.albañil) {
            return 2;
        } else if (selectedId == R.id.obra) {
            return 3;
        } else {
            throw new IllegalArgumentException("Puesto inválido");
        }
    }

}
